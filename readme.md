# inp2cpa
A script for the automatic creation of CPA files to be used by the the [epanetCPA](https://github.com/rtaormina/epanetCPA) MATLAB� toolbox.

## Installation
  - Create a virtual environment for inp2cpa using Python 3.6.4 as target interpreter.
  - From within the virtual environment execute:   ```pip install fbs PyQt5==5.9.2 wntr```
  - Clone the repository: ```git clone https://bitbucket.org/team_makro/inp2cpa.git```. You need a git bash environment to execute this command. In Windows you can download and use a bash emulator such as Git for Windows (https://gitforwindows.org/)
  - Change to the root directory and execute: ```fbs run```

## Author
Dionisis Nikolopoulos, nikolopoulos.dio@gmail.com

## License
MIT

